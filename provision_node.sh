#!/bin/bash

apt-get install -y apache2 mysql-client libapache2-mod-php5 php5 php5-cli php5-mysql php5-curl curl php5-intl git
a2enmod rewrite

cat << EOF > /etc/apache2/sites-available/node.conf
ServerName localhost

<VirtualHost *:80>
  DocumentRoot /srv/node/web

  <Directory /srv/node/web>
    Options Indexes FollowSymLinks
    AllowOverride All
    Require all granted
  </Directory>

</VirtualHost>
EOF

a2ensite node.conf
a2dissite 000-default.conf

cat << EOF > /etc/php5/apache2/conf.d/30-node.ini
date.timezone = "Europe/Kiev"
EOF

service apache2 restart

cd /srv/node
sudo -u www-data php composer.phar install
sudo -u www-data php app/console assets:install
